package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/lercher/kopen/back"
	"github.com/lercher/kopen/server"
)

var (
	flagAssets = flag.String("assets", "assets", "path to directory containing static http assets besides /api/ routes.")
	flagPort   = flag.Int("port", 8080, "http port to listen on, if 0 only the database is set up and closed")
)

func main() {
	log.Println("This is Kopen (C) 2023 by Martin Lercher")

	flag.Parse()

	foldername := "data"
	if flag.NArg() == 1 {
		foldername = flag.Arg(0)
	}
	b := back.New(foldername)

	log.Println("setting up database:", b.Name())
	db, err := b.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := db.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	err = db.SetupSchema()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("done setting up db")

	if port := *flagPort; port > 0 {
		srv := server.Server{
			DB:         db,
			AssetsPath: *flagAssets,
		}

		hs := &http.Server{
			Handler: server.NewRouter(srv),
			Addr:    fmt.Sprint(":", port),
			// Good practice: enforce timeouts for servers you create!
			WriteTimeout:      60 * time.Second,
			ReadTimeout:       60 * time.Second,
			IdleTimeout:       60 * time.Second,
			ReadHeaderTimeout: 30 * time.Second,
		}

		// ^C handler
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		go func() {
			<-c
			log.Println("Sig-Int received. Stopping services ...")
			err := hs.Close()
			if err != nil {
				log.Println(err)
			}
			err = db.Close()
			if err != nil {
				log.Println(err)
			}
			os.Exit(0)
		}()

		log.Print("Listening on http://localhost", hs.Addr, "/ ...")
		log.Println(hs.ListenAndServe()) // ListenAndServe won't return normally
	}
}
