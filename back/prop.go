package back

// Prop represents a Kopen property list
// which is persisted in the db
type Prop struct {
	// if changed also change the schema setup in back\setup.go
	Name        string   `json:"name"`
	Label       string   `json:"label"`
	Description string   `json:"description"`
	TNames      []string `json:"tnames,omitempty"`
	VNames      []string `json:"vnames,omitempty"`
	Vals        []Item   `json:"vals,omitempty"`
}
