package back

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"

	"github.com/genjidb/genji"
	"github.com/genjidb/genji/types"
)

// DB represents an Opened backend
type DB struct {
	Backend
	genjidb *genji.DB
}

// Close closes the underlying genji db
func (db DB) Close() error {
	return db.genjidb.Close()
}

// WithContext wraps the genji DB in a Context
func (db DB) WithContext(ctx context.Context) DB {
	return DB{
		Backend: db.Backend,
		genjidb: db.genjidb.WithContext(ctx),
	}
}

var errDone = errors.New("done")

// GetSingle returns the value of one parameter of the first single row
// of a query. It is an error if qy returns zero rows.
// If it returns more than one row the following rows are ignored.
func GetSingle[T any](db DB, par, qy string, args ...any) (T, error) {
	var t T
	res, err := db.genjidb.Query(qy, args...)
	if err != nil {
		return t, fmt.Errorf("%v: %v", qy, err)
	}
	defer res.Close()

	d, err := first(res)
	if err != nil {
		return t, fmt.Errorf("get first document: %v: %v", qy, err)
	}
	val, err := d.GetByField(par)
	if err != nil {
		return t, fmt.Errorf("get field value %v: %v", par, err)
	}
	if v, ok := val.V().(T); ok {
		t = v
	} else {
		return t, fmt.Errorf("not an %T: %v (%v/%T)", t, val.String(), val.Type().String(), val.V())
	}

	return t, nil
}

// StreamJSONArray writes all documents to w
// in form of a JSON array of objects plus a NL
func (db DB) StreamJSONArray(w io.Writer, qy string, args ...any) error {
	res, err := db.genjidb.Query(qy, args...)
	if err != nil {
		return fmt.Errorf("%v: %v", qy, err)
	}
	defer res.Close()

	_, err = fmt.Fprint(w, "[")
	if err != nil {
		return fmt.Errorf("%v: %v", qy, err)
	}

	n := 0
	err = res.Iterate(func(d types.Document) error {
		if n != 0 {
			_, err = fmt.Fprint(w, ",")
			if err != nil {
				return fmt.Errorf("writing comma #%v: %v", n, err)
			}
		}
		n++
		b, err := d.MarshalJSON()
		if err != nil {
			return fmt.Errorf("marshaling #%v: %v", n, err)
		}
		_, err = w.Write(b)
		if err != nil {
			return fmt.Errorf("writing #%v: %v", n, err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("iterate %v: %v", qy, err)
	}

	_, err = fmt.Fprint(w, "]")
	if err != nil {
		return fmt.Errorf("%v: %v", qy, err)
	}

	return nil
}

// StreamJSONFirst writes the first document plus a NL to w.
// It is an error if qy returns zero rows.
// If it returns more than one row the following rows are ignored.
func (db DB) StreamJSONFirst(w io.Writer, qy string, args ...any) error {
	res, err := db.genjidb.Query(qy, args...)
	if err != nil {
		return fmt.Errorf("%v: %v", qy, err)
	}
	defer res.Close()

	gotOne := false
	err = res.Iterate(func(d types.Document) error {
		b, err := d.MarshalJSON()
		if err != nil {
			return fmt.Errorf("marshaling: %v", err)
		}
		_, err = w.Write(b)
		if err != nil {
			return fmt.Errorf("writing: %v", err)
		}
		_, err = fmt.Fprintln(w)
		if err != nil {
			return fmt.Errorf("writing NL: %v", err)
		}
		gotOne = true
		return errDone
	})
	if err != nil && err != errDone {
		return fmt.Errorf("iterate %v: %v", qy, err)
	}
	if !gotOne {
		return fmt.Errorf("zero rows: %v", qy)
	}

	return nil
}

// ClientNew saves a new Client read from JSON stream to the db, which generates a new id
func (db DB) ClientNew(r io.Reader) (int64, error) {
	dec := json.NewDecoder(r)
	c := Client{Id: 0}
	err := dec.Decode(&c)
	if err != nil {
		return -1, fmt.Errorf("can't decode into client: %v", err)
	}

	tx, err := db.genjidb.Begin(true)
	if err != nil {
		return -1, fmt.Errorf("client new begin tx %v: %v", c.Name, err)
	}
	defer tx.Rollback()

	res, err := tx.Query("INSERT INTO client (name, optionals) VALUES (?, ?) RETURNING id", &c.Name, &c.Optionals)
	if err != nil {
		return -1, fmt.Errorf("client new insert %v: %v", c.Name, err)
	}
	doc, err := first(res)
	if err != nil {
		return -1, fmt.Errorf("client new first %v: %v", c.Name, err)
	}
	idVal, err := doc.GetByField("id")
	if err != nil {
		return -1, fmt.Errorf("client new id %v: %v", c.Name, err)
	}
	id := idVal.V().(int64)

	err = tx.Commit()
	if err != nil {
		return -1, fmt.Errorf("client new commit %v: %v", c.Name, err)
	}

	return id, nil
}

// ClientStore saves a Client read from JSON stream to the db updating the one with matching id
func (db DB) ClientStore(id int64, r io.Reader) error {
	dec := json.NewDecoder(r)
	c := Client{Id: id}
	err := dec.Decode(&c)
	if err != nil {
		return fmt.Errorf("can't decode into client: %v", err)
	}

	err = db.genjidb.Exec("UPDATE client SET name = ?, optionals = ? WHERE id = ?", &c.Name, &c.Optionals, &c.Id)
	if err != nil {
		return fmt.Errorf("client store update %v: %v", c.Id, err)
	}

	return nil
}

// PropStore saves a Prop read from JSON stream to the db replacing the one with name
func (db DB) PropStore(name string, r io.Reader) error {
	dec := json.NewDecoder(r)
	var p Prop
	err := dec.Decode(&p)
	if err != nil {
		return fmt.Errorf("can't decode into prop: %v", err)
	}

	err = ValidateName(name)
	if err != nil {
		return fmt.Errorf("save prop, key: %v", err)
	}

	err = ValidateName(p.Name)
	if err != nil {
		return fmt.Errorf("save prop, prop name: %v", err)
	}

	err = db.PropStoreAny(name, p)
	if err != nil {
		return err
	}

	{
		log.Println("Stored Prop:")
		err = db.StreamJSONFirst(log.Writer(), "select * from prop where name=?", p.Name)
		if err != nil {
			log.Println(err)
		}
	}

	return nil
}

// PropStoreAny saves a Prop to the db without validity check
func (db DB) PropStoreAny(name string, p Prop) error {
	tx, err := db.genjidb.Begin(true)
	if err != nil {
		return fmt.Errorf("save prop begin tx %v: %v", p.Name, err)
	}
	defer tx.Rollback()

	if name != NameForNewItem {
		err = tx.Exec("delete from prop where name=?", name)
		if err != nil {
			return fmt.Errorf("save prop remove by key %v: %v", name, err)
		}
	}

	if p.Name != NameForNewItem {
		err = tx.Exec("INSERT INTO prop VALUES ?", &p)
		if err != nil {
			return fmt.Errorf("save prop insert %v: %v", p.Name, err)
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("save prop commit %v: %v", p.Name, err)
	}

	return nil
}

func first(res *genji.Result) (types.Document, error) {
	var doc types.Document
	gotOne := false
	err := res.Iterate(func(d types.Document) error {
		doc = d
		gotOne = true
		return errDone
	})
	if err != nil && err != errDone {
		return doc, fmt.Errorf("iterate: %v", err)
	}
	if !gotOne {
		return doc, fmt.Errorf("zero rows")
	}
	return doc, nil
}
