package back

import (
	"fmt"
	"regexp"
)

var identifier = regexp.MustCompile("^[a-zA-Z](-?[a-zA-Z0-9]+)*$")

// NameForNewItem is a name which is valid but won't be stored nor deleted
const NameForNewItem = "*new*"

// ValidateName checks if the name is a valid identifier
// containig only a-z, digits and inner dashes.
// "*new*" is also valid.
func ValidateName(name string) error {
	if name == NameForNewItem || identifier.MatchString(name) {
		return nil
	}
	return fmt.Errorf("not a valid identifier name: %q", name)
}
