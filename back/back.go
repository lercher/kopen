package back

import (
	"github.com/genjidb/genji"
)

// Backend refers to the genji database folder
type Backend string

// New creates a new Backend by providing a folder name
func New(foldername string) Backend {
	return Backend(foldername)
}

// Name returns the genji folder name of the database in use
func (b Backend) Name() string {
	return string(b)
}

// Open connects to the database. The returned db needs to be closed.
func (b Backend) Open() (DB, error) {
	d := DB{Backend: b}
	db, err := genji.Open(b.Name())
	if err != nil {
		return d, err
	}
	d.genjidb = db
	return d, nil
}
