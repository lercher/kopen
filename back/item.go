package back

// Item represents an entry within a Prop list including metadata
type Item struct {
	// if changed also change the schema setup in back\setup.go
	Name string    `json:"name"`
	T    []string  `json:"t,omitempty"`
	V    []float64 `json:"v,omitempty"`
}
