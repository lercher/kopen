package back

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"
)

func setup(t *testing.T) DB {
	t.Helper()

	b := New(":memory:")
	db, err := b.Open()
	if err != nil {
		t.Fatal(err)
	}

	err = db.SetupSchema()
	if err != nil {
		t.Fatal(err)
	}

	return db
}

func saveAndLoadAgain(t *testing.T, p Prop) {
	//t.Helper()

	db := setup(t)
	defer db.Close()

	// 1st
	err := db.PropStoreAny(p.Name, p)
	if err != nil {
		t.Fatal(err)
	}

	// 2nd - overwrite
	if p.Description != "" {
		p.Description += "-sth-else"
	}
	err = db.PropStoreAny(p.Name, p)
	if err != nil {
		t.Fatal(err)
	}

	// 3rd - duplicate with different name
	q := p
	q.Name += "-duplicate"
	err = db.PropStoreAny(q.Name, q)
	if err != nil {
		t.Fatal(err)
	}

	wantBytes, _ := json.Marshal(p)
	want := string(wantBytes)
	want = strings.ReplaceAll(want, " ", "")

	qy := "select * from prop where name=?"
	buf := new(bytes.Buffer)
	err = db.StreamJSONFirst(buf, qy, p.Name)
	if err != nil {
		t.Fatal(err)
	}
	got := buf.String()
	got = strings.ReplaceAll(got, " ", "")
	got = strings.TrimSpace(got)
	t.Log(got)

	if got != want {
		t.Errorf("load from db:\ngot  %v\nwant %v", got, want)
	}

	buf.Reset()
	err = db.StreamJSONArray(buf, "select name from prop order by name")
	if err != nil {
		t.Fatal(err)
	}
	got = buf.String()
	want = fmt.Sprintf(`[{"name": %q},{"name": %q}]`, p.Name, q.Name)
	if got != want {
		t.Errorf("load array from db:\ngot  %v\nwant %v", got, want)
	}

}

func TestDB_SaveAndLoadProps(t *testing.T) {
	t.Log(`This test fails if Prop/Item is out of sync with SetupSchema in back\setup.go`)
	saveAndLoadAgain(t, Prop{
		Name:        "an-arbitrary-prop",
		Description: "a-description",
		TNames:      []string{"aname", "bname", "cname"},
		VNames:      []string{"1name", "2name", "3name", "4name"},
		Vals: []Item{
			{
				Name: "1st-val",
				T:    []string{"a", "b", "c"},
				V:    []float64{1.1, -2.2, 3.333, -4.4444},
			},
			{
				Name: "2nd-val",
				T:    []string{"A", "B", "C"},
				V:    []float64{0, 1, 2, 3},
			},
		},
	})

	saveAndLoadAgain(t, Prop{
		Name: "1st-empty",
	})

	saveAndLoadAgain(t, Prop{
		Name:   "2nd-empty",
		TNames: []string{"a", "b", "c", "d"},
	})
}
