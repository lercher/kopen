package back

type Client struct {
	// if changed also change the schema setup in back\setup.go
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	Optionals []struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	} `json:"optionals,omitempty"`
}
