package back

import (
	"fmt"
	"log"
)

// SetupSchema creates neccessary DB schema objects for later use
// unless they already exist
func (db DB) SetupSchema() error {
	var err error
	var n int64

	err = db.genjidb.Exec(`
        CREATE TABLE IF NOT EXISTS prop (
            name TEXT PRIMARY KEY,
			label TEXT,
			description TEXT,
            tnames ARRAY,
			vnames ARRAY,
			vals ARRAY, -- name, t[], v[]
			...
        )
    `)
	if err != nil {
		return fmt.Errorf("creating table prop: %v", err)
	}

	n, err = GetSingle[int64](db, "n", "select count(*) as n from prop")
	if err != nil {
		return fmt.Errorf("counting prop: %v", err)
	}
	log.Println("We have", n, "props in the database", db.Name())

	err = db.genjidb.Exec(`
        CREATE TABLE IF NOT EXISTS schema (
            name TEXT PRIMARY KEY,
			description TEXT,
			...
        )
    `)
	if err != nil {
		return fmt.Errorf("creating table prop: %v", err)
	}

	n, err = GetSingle[int64](db, "n", "select count(*) as n from schema")
	if err != nil {
		return fmt.Errorf("counting schema: %v", err)
	}
	log.Println("We have", n, "schemas in the database", db.Name())

	err = db.genjidb.Exec(`
		CREATE SEQUENCE IF NOT EXISTS client_id AS INTEGER START WITH 1 INCREMENT BY 1
	`)
	if err != nil {
		return fmt.Errorf("creating sequence client_id: %v", err)
	}

	err = db.genjidb.Exec(`
		CREATE TABLE IF NOT EXISTS client (
			id INTEGER PRIMARY KEY DEFAULT (NEXT VALUE FOR client_id),
			name TEXT,
			optionals ARRAY,
			...
		)
	`)
	if err != nil {
		return fmt.Errorf("creating table client: %v", err)
	}

	n, err = GetSingle[int64](db, "n", "select count(*) as n from client")
	if err != nil {
		return fmt.Errorf("counting client: %v", err)
	}
	log.Println("We have", n, "clients in the database", db.Name())

	return nil
}
