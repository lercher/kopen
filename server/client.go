package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func (s Server) ClientList(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	err := db.StreamJSONArray(w, "SELECT * FROM client ORDER BY name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s Server) Client(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	idStr, err := Var(r, "id")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	qy := "SELECT * FROM client WHERE id = ?"
	err = db.StreamJSONFirst(w, qy, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s Server) ClientNew(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	id, err := db.ClientNew(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	log.Default().Printf("New client with id %d", id)

	_ = json.NewEncoder(w).Encode(map[string]int64{"id": id})
}

func (s Server) ClientPut(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	idStr, err := Var(r, "id")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = db.ClientStore(id, r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, _ = fmt.Fprintln(w, "OK")
}
