package server

import (
	"github.com/lercher/kopen/back"
)

// Server implements http API services based
// on our backend
type Server struct {
	DB         back.DB
	AssetsPath string
}
