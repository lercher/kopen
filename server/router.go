package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// NewRouter creates a new http Router with configured
// middleware and http routes
func NewRouter(s Server) *mux.Router {
	router := mux.NewRouter()
	router.Use(handlers.CompressHandler)

	api := router.PathPrefix("/api").Subrouter()

	// property lists
	api.HandleFunc("/prop/", s.PropList).Methods("GET")
	api.HandleFunc("/prop/{name}/", s.Prop).Methods("GET")
	api.HandleFunc("/prop/{name}/", s.PropPut).Methods("POST")

	// schemas
	api.HandleFunc("/schema/", s.SchemaList).Methods("GET")

	// clients
	api.HandleFunc("/client/", s.ClientList).Methods("GET")
	api.HandleFunc("/client/{id:[0-9]+}/", s.Client).Methods("GET")
	api.HandleFunc("/client/{id:[0-9]+}/", s.ClientPut).Methods("POST")
	api.HandleFunc("/client/*new*/", s.ClientNew).Methods("POST")

	// catch all routes, must be last!
	// we don't use router.Use(cacheControlMaxAgeHeaderMiddlewareFactory(1h)) here, b/c it would affect all other handlers.
	router.PathPrefix("/").Methods("GET").Handler(cacheControl(time.Hour)(http.FileServer(http.Dir(s.AssetsPath))))

	return router
}

func cacheControl(d time.Duration) func(next http.Handler) http.Handler {
	maxage := fmt.Sprintf("max-age=%d", int(d.Seconds()))
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Cache-Control", maxage) // 86400 = 24h, 3600 = 1h
			next.ServeHTTP(w, r)
		})
	}
}
