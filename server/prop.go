package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/lercher/kopen/back"
)

// Prop serves a particular property
func (s Server) Prop(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	name, err := Var(r, "name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if name == "*new*" {
		enc := json.NewEncoder(w)
		_ = enc.Encode(back.Prop{
			Name: "Name",
			Vals: []back.Item{
				{Name: "Wert 1"},
				{Name: "Wert 2"},
			},
		})
		return
	}

	err = back.ValidateName(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	qy := "select * from prop where name=?"
	err = db.StreamJSONFirst(w, qy, name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// PropPut saves a particular property from the body of the response
// identified by name
func (s Server) PropPut(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	name, err := Var(r, "name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = back.ValidateName(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = db.PropStore(name, r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, _ = fmt.Fprintln(w, "OK")
}

// PropList lists Name, Label and Description of all Props stored ordered by name
func (s Server) PropList(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	err := db.StreamJSONArray(w, "select name, label, description from prop order by name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
