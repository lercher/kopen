package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// Var retrieves by its name either a form field value, a query param or a route var.
// If name is "#all#" then a Sprint of all mux variables is returned.
func Var(r *http.Request, name string) (string, error) {
	v := r.FormValue(name)
	if v != "" {
		return v, nil
	}
	v = r.URL.Query().Get(name)
	if v != "" {
		return v, nil
	}
	muxvars := mux.Vars(r)
	if name == "#all#" {
		return fmt.Sprint(muxvars), nil
	}
	v, ok := muxvars[name]
	if !ok {
		return "", fmt.Errorf("key %q not found in %v", name, muxvars)
	}
	return v, nil
}
