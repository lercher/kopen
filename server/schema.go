package server

import "net/http"

// SchemaList lists Name and Description of all Schemas stored ordered by name
func (s Server) SchemaList(w http.ResponseWriter, r *http.Request) {
	db := s.DB.WithContext(r.Context())

	err := db.StreamJSONArray(w, "select name, description from schema order by name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
