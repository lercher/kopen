const api = "/api";
const client = "/client/";
const edit = "/edit";
const prop = "/prop/";
const schema = "/schema/";
const validName = /^[a-zA-Z](-?[a-zA-Z0-9]+)*$/;
const magicClientPropName = "Kundendaten"

function moveArrayItem(array, index, delta) {
    const target = index+delta;
    const l = array.length;
    if (0 <= target && target < l && 0 <= index && index < l ) {
        const hand = array.splice(index, 1);
        if (delta !== 0) {
            array.splice(target, 0, hand[0]);
        }
    }
}

angular.module('components', ['ngRoute'])

    .component("configWarning", {
        template: "<p>Konfigurationsdaten &ndash; Handle with care</p>"
    })

    .component("clientList", {
        templateUrl: "/clientlist.html",
        controller: function ($http) {
            $http.get(api+client).then(d => {
                this.clients = d.data;
            }, console.log);
        }
    })

    .component("clientDetail", {
        templateUrl: "/clientdetail.html",
        controller: function ($http, $routeParams) {
            this.clientId = $routeParams.clientId;
            $http.get(api+client+this.clientId+"/").then(d => {
                this.client = d.data;
            }, console.log);
        }
    })

    .component("clientEdit", {
        templateUrl: "/clientedit.html",
        controller: function ($http, $routeParams, $location) {
            this.clientId = $routeParams.clientId;
            const path = client+this.clientId+"/";
            
            this.options = [];
            this.client = {
                name: "",
                optionals: [],
            };

            this.appendLine = () => {
                this.client.optionals.push({name: "", value: ""});
            };

            this.moveLine = (index, delta) => {
                moveArrayItem(this.client.optionals, index, delta);
            };

            this.cancel = () => {
                $location.path(this.clientId === "*new*" ? client : path);
            };

            this.save = (withBack) => {
                $http.post(api+path, this.client).then(d => {
                    if (this.clientId === "*new*") {
                        this.clientId = d.data.id;
                    }
                    const newPath = client+this.clientId+"/";
                    if (withBack) {
                        $location.path(newPath);
                    } else {
                        $location.path(edit+newPath);
                    }
                }, console.log);
            };

            $http.get(api+prop+magicClientPropName+"/").then(d => {
                this.options = d.data.vals.map(v => v.name);
            }, console.log);

            if (this.clientId !== "*new*") {
                $http.get(api+path).then(d => {
                    this.client = d.data;
                }, console.log);
            }
        }
    })

    .component('schemaList', {
        templateUrl: '/schemalist.html',
        controller: function ($http) {
            $http.get(api+schema).then(d => {
                this.schemas = d.data;
            }, console.log);
        }
    })

    .component('schemaEdit', {
        templateUrl: '/schemaedit.html',
        controller: function ($http, $routeParams, $location) {
            this.name = $routeParams.name;            
            const path = schema+this.name+"/";
            const back = schema;

            this.validName = validName;

            this.schema = {
                name: "",
                description: "",
                props: [],
            };

            this.cancel = () => {
                $location.path(back);
            };

            this.save = (withBack) => {
                const path2 = schema+this.schema.name+"/";
                $http.post(api+path, this.prop).then(d => {
                    if (withBack) {
                        $location.path(back);
                    } else {
                        $location.path(edit+path2);
                    }
                }, console.log);
            };

            $http.get(api+path).then(d => {
                this.schema = d.data;
                this.schema.props ||= [];
            }, console.log);
            $http.get(api+prop).then(d => {
                this.props = d.data;
            }, console.log);

            this.addProp = (pl) => {
                if (!this.hasProp(pl.name)) {                    
                    this.schema.props.push({
                        writeable: true,
                        ...pl,
                    });
                }
            };

            this.hasProp = (name) => {
                return this.schema.props.some((prop) => prop.name === name);
            }
        }
    })

    .component('propList', {
        templateUrl: '/proplist.html',
        controller: function ($http) {
            $http.get(api+prop).then(d => {
                this.props = d.data;
            }, console.log);
        }
    })

    .component('propDetail', {
        templateUrl: '/propdetail.html',
        controller: function ($http, $routeParams) {
            this.name = $routeParams.name;

            $http.get(api+prop + this.name + "/").then(d => {
                this.prop = d.data;
            }, console.log);
        }
    })

    .component('propEdit', {
        templateUrl: '/propedit.html',
        controller: function ($http, $routeParams, $location) {
            this.name = $routeParams.name;            
            const path = prop+this.name+"/";

            this.validName = validName;

            this.appendLine = () => {
                let item = {name: "", t: [], v: []};
                this.prop.tnames.forEach(() => item.t.push(""));
                this.prop.vnames.forEach(() => item.v.push(0));
                this.prop.vals.push(item);
            }

            this.moveLine = (index, delta) => {
                moveArrayItem(this.prop.vals, index, delta);
            }

            this.addt = () => {
                this.prop.tnames.push("");
                this.prop.vals.forEach((v) => {
                    v.t ||= [];
                    v.t.length = this.prop.tnames.length;
                });
            }

            this.addv = () => {
                this.prop.vnames.push("");
                this.prop.vals.forEach((v) => {
                    v.v ||= [];
                    v.v.length = this.prop.vnames.length;
                });
            }

            this.deltCol = (idx) => {
                this.prop.tnames.splice(idx, 1);
                this.prop.vals.forEach((v) => {
                    v.t ||= [];
                    v.t.splice(idx, 1);
                });
            }

            this.delvCol = (idx) => {
                this.prop.vnames.splice(idx, 1);
                this.prop.vals.forEach((v) => {
                    v.v ||= [];
                    v.v.splice(idx, 1);
                });
            }

            this.cancel = () => {
                const back = this.name === '*new*' ? prop : path;
                $location.path(back);
            };

            this.save = (withBack) => {
                const path2 = prop+this.prop.name+"/";
                $http.post(api+path, this.prop).then(d => {
                    if (withBack) {
                        $location.path(path2);
                    } else {
                        $location.path(edit+path2);
                    }
                }, console.log);
            };

            $http.get(api+path).then(d => {
                this.prop = d.data;
                this.prop.tnames ||= [];
                this.prop.vnames ||= [];
                this.prop.vals ||= [];
            }, console.log);
        }
    })

    ;