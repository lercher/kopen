angular.module('kopenapp', ['components', 'ngRoute'])
    .config(function ($routeProvider) {
        const template = (html) => {return { template: '<.></.>'.replace('.', html) }};
        $routeProvider.
            when('/prop/', template('prop-list')).
            when('/prop/:name/', template('prop-detail')).
            when('/edit/prop/:name/', template('prop-edit')).
            when('/client/', template('client-list')).
            when('/client/:clientId/', template('client-detail')).
            when('/edit/client/:clientId/', template('client-edit')).
            when('/catalog/', template('catalog-list')).
            when('/catalog/:catalogId/', template('catalog-detail')).
            when('/project/', template('project-list')).
            when('/project/:projectId/', template('project-detail')).
            when('/schema/', template('schema-list')).
            when('/edit/schema/:name/', template('schema-edit')).
            otherwise('/project/');
        /*
        */
    })

    .controller('props', function ($scope, $http) {
        $scope.props = [{ name: "(laden)", description: "..." }];

        $http.get("/api/prop/").then(d => {
            $scope.props = d.data;
        }, console.log);
    })
    ;