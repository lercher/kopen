# Kopen - Plan and track your craftsman projects

Kopen is a UI app to plan and manage craftsman projects
and finally create delivery slips and invoices from them.

## Installation

TBD. A simple

```bat
set CGO_ENABLED=0
go run github.com/lercher/kopen@latest
```

should be sufficient if a local Go installation is present.

Note: There were CGo errors  reported in pebble during build,
if CGo is enabled and there is no C/C++ compiler in the
path. It's safe to turn it off, however.

## Usage

TBD

Central areas of interest are:

- **Project** - Main area of interest which associates a client, a list of rooms or parts
  of work and a list of catalog items for each room
- **Client** - record and store adresses of your clients
- **Catalog** - build a reusable list of components you need to build projects
- Finally for configuration, **Props** - define attributed lists of values like colors,
  structures, variants and attribute them to do calculations on them.
  Each Catalog item can have a variable list of such props.
- And **Schema** - binding a name to a list of _Props_, marking them r/w or r/o. Schemas
  are associated with a catalog item allowing to specify r/w and r/o values.
  When a catalog item is pulled (i.e. copied) into a project, r/o values are only displayed but
  r/w items can be changed inside a project item.

Please note that we intentionally do a lot of copying instead of referencing.
So basicly think of a project to be a huge word processor document with a lot
of help filling all the parts. Once created, you work with it by using and adding new
projective views on it, like a list of deliery slips and finally an invoice.

## Support

If you seek out for help, use gitlab's issue tracker here.

## Roadmap

- getting started with the code
- create a roadmap of features

## Contributing

I'm open to contributions as long as the code remains simple and generic.
Be sure to add some useful unit tests and run them all together.
I.e. this command should be sucessful:

```sh
go test ./...
```

## Authors and Acknowledgment

We value any contribution. Special thanks got to:

- Lerbert

## License

This repo is licensend under the MIT License.

## Project status

Experimental and Incubating. Fields of interest:

- Props - impression of the final form
- Schema - design, experimental code
- Client - none
- Catalogue - none
- Project - none

## Ressources

TBD

- see in the footer of the running web app
- <https://genji.dev/docs/essentials/sql-introduction/>
